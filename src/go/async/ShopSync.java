package go.async;

import java.util.Random;

public class ShopSync {

	/**
	 * 商店名称
	 */
	private String name;
	private Random random = new Random();

	public ShopSync(String name) {
        this.name = name;
    }

	public String getName() {
		return name;
	}

	/**
	 * (阻塞式)通过名称查询价格
	 *
	 * @param product
	 * @return
	 */
	public double getPrice(String product) {
		return calculatePrice(product);
	}

	/**
	 * 计算价格(模拟一个产生价格的方法)
	 *
	 * @param product
	 * @return
	 */
	private double calculatePrice(String product) {
		delay();
		// 数字*字符=数字(产生价格的方法)
		return random.nextDouble() * product.charAt(0) * product.charAt(1);
	}

	/**
	 * 模拟耗时操作,阻塞1秒
	 */
	private void delay() {
		try {
			Thread.sleep(1_000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
