package go.async;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.stream.Collectors;

public class ClientTest {

	static List<ShopSync> shops = Arrays.asList(new ShopSync("淘宝"), new ShopSync("天猫"), new ShopSync("京东"),
			new ShopSync("亚马逊"));

	public static void main(String[] args) {
		//同步调用，一个一个来
		long start = System.nanoTime();
		List<String> list = findPriceSync("iphone666s");
		System.out.println(list);
		System.out.println("Done in " + (System.nanoTime() - start) / 1_000_000 + " ms");
		//并行查询
	    start = System.nanoTime();
		List<String> listParallel = findPriceParallel("iphone666s");
		System.out.println(listParallel);
		System.out.println("Done in " + (System.nanoTime() - start) / 1_000_000 + " ms");
		//异步调用
	    start = System.nanoTime();
		List<String> listAsync = findPriceAsync("iphone666s");
		System.out.println(listAsync);
		System.out.println("Done in " + (System.nanoTime() - start) / 1_000_000 + " ms");
		//异步调用,调整线程池
	    start = System.nanoTime();
		List<String> listAsyncPool = findPriceAsyncPool("iphone666s");
		System.out.println(listAsyncPool);
		System.out.println("Done in " + (System.nanoTime() - start) / 1_000_000 + " ms");
	}



	/**
     * 使用并行流对请求进行并行操作
     * @param product
     * @return
     */
    public static List<String> findPriceParallel(String product) {
        List<String> list = shops.parallelStream()
                .map(shop ->
                        String.format("%s price is %.2f RMB",
                                shop.getName(),
                                shop.getPrice(product)))

                .collect(Collectors.toList());

        return list;
    }

	/**
	 *
	 * @param product 商品名称
	 * @return 根据名字返回每个商店的商品价格
	 */
	public static List<String> findPriceSync(String product) {
		List<String> list = shops.stream()
				.map(shop -> String.format("%s price is %.2f RMB", shop.getName(), shop.getPrice(product)))

				.collect(Collectors.toList());

		return list;
	}

	/**
     * 使用 CompletableFuture 发起异步请求
     * @param product
     * @return
     */
    public static List<String> findPriceAsync(String product) {
        List<CompletableFuture<String>> futures = shops.stream()
                .map(shop -> CompletableFuture.supplyAsync(
                        () -> String.format("%s price is %.2f RMB",
                                shop.getName(),
                                shop.getPrice(product)))
                )
                .collect(Collectors.toList());
        List<String> list = futures.stream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList());


        return list;
    }

    /**
     * 使用定制的 Executor 配置 CompletableFuture
     *
     * @param product
     * @return
     */
    public static List<String> findPriceAsyncPool(String product) {

        //为“最优价格查询器”应用定制的执行器 Execotor
        Executor executor = Executors.newFixedThreadPool(Math.min(shops.size(), 100),
                new ThreadFactory() {
                    @Override
                    public Thread newThread(Runnable r) {
                        Thread thread = new Thread(r);
                        //使用守护线程,使用这种方式不会组织程序的关停
                        thread.setDaemon(true);
                        return thread;
                    }
                }
        );

      //将执行器Execotor 作为第二个参数传递给 supplyAsync 工厂方法
        List<CompletableFuture<String>> futures = shops.stream()
                .map(shop -> CompletableFuture.supplyAsync(
                        () -> String.format("%s price is %.2f RMB",
                                shop.getName(),
                                shop.getPrice(product)), executor)
                )
                .collect(Collectors.toList());
        List<String> list = futures.stream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList());


        return list;
    }

}
