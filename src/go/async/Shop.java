package go.async;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public class Shop {

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private String name;

	public Shop(String name) {
		this.name = name;

	}

	/**
	 * (阻塞式)通过名称查询价格
	 *
	 * @param product
	 * @return
	 */
	public double getPrice(String product) {
		return calculatePrice(product);
	}

	/**
	 * 计算价格
	 *
	 * @param product
	 * @return
	 */
	private double calculatePrice(String product) {
		delay();
		// 数字*字符=数字
		return 10 * product.charAt(0);

	}

	/**
	 * 模拟耗时操作,阻塞1秒
	 */
	private void delay() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * (非阻塞式)异步获取价格
	 *
	 * @param product
	 * @return
	 */
	public Future<Double> getPriceAsync(String product) {
		CompletableFuture<Double> future = new CompletableFuture<>();
		new Thread(() -> {
			double price = calculatePrice(product);
			// 需要长时间计算的任务结束并返回结果时,设置Future返回值
			future.complete(price);
		}).start();

		// 无需等待还没结束的计算,直接返回Future对象
		return future;
	}

	/**
	 * 使用静态工厂supplyAsync(非阻塞式)异步获取价格
	 *
	 * @param product
	 * @return
	 */
	public Future<Double> getPriceAsync2(String product) {
		CompletableFuture<Double> future = CompletableFuture.supplyAsync(() -> calculatePrice(product));

		// 无需等待还没结束的计算,直接返回Future对象
		return future;
	}

	public Future<Double> getPriceAsyncWithException(String product) {
		CompletableFuture<Double> future = new CompletableFuture<>();
		new Thread(() -> {
			double price = calculatePriceWithException(product);
			// 需要长时间计算的任务结束并返回结果时,设置Future返回值
			future.complete(price);
		}).start();

		// 无需等待还没结束的计算,直接返回Future对象
		return future;
	}



	private double calculatePriceWithException(String product) {
		delay();
		int i = 1 / 0;// 故意抛出 java.lang.ArithmeticException: / by zero 异常
		// 数字*字符=数字(产生价格的方法)
		return 10 * product.charAt(0);
	}



}
